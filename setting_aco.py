from urllib.request import urlopen
import json
from connect_db import Connect_db
from path_url import Path_URL

connect_db = Connect_db
path_file = Path_URL

url = path_file.path_server+"api/Rest_api/get_aco"

class Setting_aco:

    def get_setting(machine_code):
        path_url = url+'?machine_code='+machine_code
        response = urlopen(path_url)
        data_json = json.loads(response.read())
        
        mydb = connect_db.update_setting_mode()

        mycursor = mydb.cursor()
        sql = "UPDATE aco SET aco_1 = "+data_json[0]['aco_1']+", aco_2 = "+data_json[0]['aco_2']+", aco_3 = "+data_json[0]['aco_3']+", aco_4 = "+data_json[0]['aco_4']+", aco_5 = "+data_json[0]['aco_5']+", aco_6 = "+data_json[0]['aco_6']+", aco_7 = "+data_json[0]['aco_7']+", aco_8 = "+data_json[0]['aco_8']+", aco_9 = "+data_json[0]['aco_9']+", aco_10 = "+data_json[0]['aco_10']+", aco_11 = "+data_json[0]['aco_11']+", aco_12 = "+data_json[0]['aco_12']+", aco_13 = "+data_json[0]['aco_13']+", aco_14 = "+data_json[0]['aco_14']+", aco_15 = "+data_json[0]['aco_15']+", aco_16 = "+data_json[0]['aco_16']+", aco_17 = "+data_json[0]['aco_17']+", aco_18 = "+data_json[0]['aco_18']+", aco_19 = "+data_json[0]['aco_19']+", aco_20 = "+data_json[0]['aco_20']+", aco_21 = "+data_json[0]['aco_21']+", aco_22 = "+data_json[0]['aco_22']+", aco_23 = "+data_json[0]['aco_23']+", aco_24 = "+data_json[0]['aco_24']+" WHERE aco_id = 1"
        
        mycursor.execute(sql)
        mydb.commit()
        # data_json[0]['sm_filtration']
